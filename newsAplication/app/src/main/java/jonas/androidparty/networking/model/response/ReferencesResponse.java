package jonas.androidparty.networking.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * class created by jonasseputis on 05/04/19
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferencesResponse {

    @JsonProperty("id")
    public String id;

    @JsonProperty("type")
    private String type;

    public String getAuthorId() {
        return id;
    }

    public String getType() {
        return type;
    }
}
