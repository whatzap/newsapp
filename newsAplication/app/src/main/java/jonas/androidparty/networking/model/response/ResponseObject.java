package jonas.androidparty.networking.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * class created by jonasseputis on 05/04/19
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseObject implements Serializable {

    @JsonProperty("currentPage")
    private Long currentPage;

    @JsonProperty("pages")
    private Long pages;

    @JsonProperty("results")
    private List<ResultsObject> results;

    public Long getCurrentPage() {
        return currentPage;
    }

    public Long getPages() {
        return pages;
    }

    public List<ResultsObject> getResults() {
        return results;
    }
}
