package jonas.androidparty.networking;

import io.reactivex.Observable;
import jonas.androidparty.networking.model.response.MainResponse;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * class created by jonasseputis on 18/11/18
 */
public interface AppApi {

    @GET("/technology")
    Observable<MainResponse> receiveNews(@Header("api-key") String token,
                                         @Query(QueryConstants.SHOW_REFERENCES) String reference,
                                         @Query(QueryConstants.PAGE_SIZE) Long pageSize,
                                         @Query(QueryConstants.PAGE) Long page);
}
