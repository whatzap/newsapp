package jonas.androidparty.networking;

/**
 * class created by jonasseputis on 05/04/19
 */
public class QueryConstants {

    public static final String SHOW_REFERENCES = "show-references";
    public static final String PAGE_SIZE = "page-size";
    public static final String PAGE = "page";
}
