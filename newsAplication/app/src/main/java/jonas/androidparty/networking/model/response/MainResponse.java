package jonas.androidparty.networking.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * class created by jonasseputis on 05/04/19
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MainResponse implements Serializable {

    @JsonProperty("response")
    public ResponseObject response;

    public ResponseObject getResponse() {
        return response;
    }

}
