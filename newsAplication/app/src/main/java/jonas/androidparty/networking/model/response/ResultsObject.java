package jonas.androidparty.networking.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * class created by jonasseputis on 05/04/19
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultsObject implements Serializable {

    @JsonProperty("sectionName")
    public String sectionName;

    @JsonProperty("webPublicationDate")
    public Date webPublicationDate;

    @JsonProperty("webTitle")
    public String webTitle;

    @JsonProperty("webUrl")
    public String webUrl;

    @JsonProperty("references")
    public List<ReferencesResponse> references;

    public String getSectionName() {
        return sectionName;
    }

    public Date getWebPublicationDate() {
        return webPublicationDate;
    }

    public String getWebTitle() {
        return webTitle;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public List<ReferencesResponse> getReferences() {
        return references;
    }
}
