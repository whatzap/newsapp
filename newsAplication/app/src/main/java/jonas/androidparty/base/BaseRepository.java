package jonas.androidparty.base;

import jonas.androidparty.networking.AppApi;

/**
 * class created by jonasseputis on 18/11/18
 */
public class BaseRepository {

    protected AppApi appApi;

    public BaseRepository(AppApi appApi) {
        this.appApi = appApi;
    }

}
