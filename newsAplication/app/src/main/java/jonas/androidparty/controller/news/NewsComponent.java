package jonas.androidparty.controller.news;

import dagger.Component;
import jonas.androidparty.di.components.IApplicationComponent;
import jonas.androidparty.di.scope.PerView;

/**
 * class created by jonasseputis on 05/04/19
 */
@Component(dependencies = IApplicationComponent.class)
@PerView
public interface NewsComponent {
    void inject(NewsController newsController);
}
