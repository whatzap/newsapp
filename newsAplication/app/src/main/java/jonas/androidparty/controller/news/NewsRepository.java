package jonas.androidparty.controller.news;

import javax.inject.Inject;

import io.reactivex.Observable;
import jonas.androidparty.BuildConfig;
import jonas.androidparty.base.BaseRepository;
import jonas.androidparty.networking.AppApi;
import jonas.androidparty.networking.model.response.MainResponse;

/**
 * class created by jonasseputis on 05/04/19
 */
public class NewsRepository extends BaseRepository {

    private static final Long PAGE_SIZE = 100L;

    @Inject
    public NewsRepository(AppApi appApi) {
        super(appApi);
    }

    public Observable<MainResponse> getNews(String references, Long page) {
        return appApi.receiveNews(BuildConfig.TOKEN, references, PAGE_SIZE, page);
    }

}
