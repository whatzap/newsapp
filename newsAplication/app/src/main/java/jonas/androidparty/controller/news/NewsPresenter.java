package jonas.androidparty.controller.news;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import jonas.androidparty.base.BasePresenter;
import jonas.androidparty.networking.model.response.MainResponse;
import jonas.androidparty.networking.sheduler.Main;

/**
 * class created by jonasseputis on 05/04/19
 */
public class NewsPresenter extends BasePresenter<NewsView> {

    private NewsRepository newsRepository;
    private Scheduler scheduler;

    @Inject
    public NewsPresenter(NewsRepository newsRepository, @Main Scheduler scheduler) {
        this.newsRepository = newsRepository;
        this.scheduler = scheduler;
    }

    public void receiveNewsList(String references, Long page) {
        newsRepository.getNews(references, page)
                .observeOn(scheduler)
                .subscribe(new Observer<MainResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("receiveNewsList", "onSubscribe");
                        getView().overlayVisible(true);
                        subscriptions.add(d);
                    }

                    @Override
                    public void onNext(MainResponse mainResponse) {
                        Log.d("receiveNewsList", "onNext");
                        getView().getMainResponse(mainResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("receiveNewsList", "onError");
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("receiveNewsList", "onComplete");
                        getView().overlayVisible(false);
                    }
                });
    }
}
