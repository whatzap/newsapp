package jonas.androidparty.controller.news;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.RouterTransaction;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jonas.androidparty.AndroidApp;
import jonas.androidparty.R;
import jonas.androidparty.adapter.NewsListAdapter;
import jonas.androidparty.controller.webview.WebViewController;
import jonas.androidparty.networking.model.response.MainResponse;
import jonas.androidparty.networking.model.response.ResultsObject;

import static jonas.androidparty.controller.webview.WebViewController.WEB_LINK;

/**
 * class created by jonasseputis on 05/04/19
 */
public class NewsController extends Controller implements NewsView, View.OnClickListener, NewsListAdapter.NewsClickListener {

    private static final String AUTHOR_VALUE = "author";
    private static final Long FIRST_PAGE = 1L;
    private static final String MAIN_RESPONSE = "main_response";
    private static final String SAVED_STATE = "saved_state";

    private NewsListAdapter newsListAdapter;
    private boolean savedState = false;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.overlay_layout)
    View overlayView;
    @BindView(R.id.current_page_text_view)
    TextView currentPage;
    @BindView(R.id.previous_page_layout)
    LinearLayout previousPage;
    @BindView(R.id.next_page_layout)
    LinearLayout nextPage;
    @BindView(R.id.previous_page_button)
    ImageButton previousPageButton;
    @BindView(R.id.next_page_button)
    ImageButton nextPageButton;

    private MainResponse mainResponse;

    @Inject
    NewsPresenter presenter;

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View root = inflater.inflate(R.layout.controller_list, container, false);
        ButterKnife.bind(this, root);
        DaggerNewsComponent.builder().iApplicationComponent(AndroidApp.getAppComponent(getActivity())).build().inject(this);
        presenter.setView(this);
        overlayView.setVisibility(View.GONE);

        if (!savedState && isNetworkAvailable())
            callFirstPage();

        if (newsListAdapter == null)
            newsListAdapter = new NewsListAdapter();

        newsListAdapter.setNewsItemClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (recyclerView != null && recyclerView.getAdapter() == null)
            recyclerView.setAdapter(newsListAdapter);

        previousPageButton.setOnClickListener(this);
        nextPageButton.setOnClickListener(this);

        return root;
    }

    @Override
    protected void onRestoreViewState(@NonNull View view, @NonNull Bundle savedViewState) {
        if (savedViewState.getBoolean(SAVED_STATE)) {
            savedState = false;
        }
        if (savedViewState.getSerializable(MAIN_RESPONSE) != null) {
            mainResponse = (MainResponse) savedViewState.getSerializable(MAIN_RESPONSE);
            if (mainResponse != null && mainResponse.getResponse() != null && mainResponse.getResponse().getResults() != null) {
                handlePageButtons();
                setCurrentPage();
                setDataToAdapter(mainResponse.getResponse().getResults());
            }
        }
        super.onRestoreViewState(view, savedViewState);
    }


    @Override
    protected void onSaveViewState(@NonNull View view, @NonNull Bundle outState) {
        outState.putSerializable(MAIN_RESPONSE, mainResponse);
        savedState = true;
        outState.putBoolean(SAVED_STATE, savedState);
        super.onSaveViewState(view, outState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(MAIN_RESPONSE, mainResponse);
        super.onSaveInstanceState(outState);
    }

    private void setCurrentPage() {
        currentPage.setText(String.valueOf(mainResponse.getResponse().getCurrentPage()));
    }

    private void handlePageButtons() {
        if (mainResponse.getResponse().getCurrentPage() == 1) {
            previousPage.setVisibility(View.INVISIBLE);
        } else {
            previousPage.setVisibility(View.VISIBLE);
        }

        if (mainResponse.getResponse().getCurrentPage().equals(mainResponse.getResponse().getPages())) {
            nextPage.setVisibility(View.INVISIBLE);
        } else {
            nextPage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void overlayVisible(boolean visible) {
        if (visible) {
            overlayView.setVisibility(View.VISIBLE);
        } else {
            overlayView.setVisibility(View.GONE);
        }
    }

    @Override
    public void getMainResponse(MainResponse mainResponse) {
        this.mainResponse = mainResponse;
        setCurrentPage();
        handlePageButtons();
        setDataToAdapter(mainResponse.getResponse().getResults());
    }

    private void setDataToAdapter(List<ResultsObject> results) {
        if (newsListAdapter != null) {
            newsListAdapter.setNewsList(results);
            newsListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previous_page_button:
                callFirstPage();
                if (mainResponse != null && mainResponse.getResponse().getCurrentPage() > 1 && isNetworkAvailable()) {
                    presenter.receiveNewsList(AUTHOR_VALUE, mainResponse.getResponse().getCurrentPage() - 1);
                }
                break;
            case R.id.next_page_button:
                callFirstPage();
                if (mainResponse != null && mainResponse.getResponse().getCurrentPage() + 1 < mainResponse.getResponse().getPages() && isNetworkAvailable()) {
                    presenter.receiveNewsList(AUTHOR_VALUE, mainResponse.getResponse().getCurrentPage() + 1);
                }
                break;
        }
    }

    private void callFirstPage() {
        if(mainResponse == null && isNetworkAvailable()) {
            presenter.receiveNewsList(AUTHOR_VALUE, FIRST_PAGE);
        }
    }

    @Override
    public void onNewsItemClick(ResultsObject resultsObject) {
        Bundle bundle = new Bundle();
        bundle.putString(WEB_LINK, resultsObject.webUrl);
        getRouter().pushController(RouterTransaction.with(new WebViewController(bundle)));
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            overlayView.setVisibility(View.GONE);
            Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
