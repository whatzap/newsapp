package jonas.androidparty.controller.news;

import jonas.androidparty.networking.model.response.MainResponse;

/**
 * class created by jonasseputis on 05/04/19
 */
public interface NewsView {

    void overlayVisible(boolean visible);

    void getMainResponse(MainResponse mainResponse);
}
