package jonas.androidparty.controller.webview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;

import com.bluelinelabs.conductor.Controller;

import butterknife.BindView;
import butterknife.ButterKnife;
import jonas.androidparty.R;
import jonas.androidparty.activity.MainActivity;

/**
 * class created by jonasseputis on 08/04/19
 */
public class WebViewController extends Controller {

    public static final String WEB_LINK = "web_link";

    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.back_button)
    ImageButton backButton;

    private String loadUrl;

    public WebViewController(@Nullable Bundle args) {
        this.loadUrl = args.getString(WEB_LINK);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View root = inflater.inflate(R.layout.controller_web_view, container, false);
        ButterKnife.bind(this, root);

        webView.loadUrl(loadUrl);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.setWebChromeClient(new WebChromeClient());
        backButton.setOnClickListener(v -> ((MainActivity) getActivity()).pressedBackToolbar());
        return root;
    }
}
