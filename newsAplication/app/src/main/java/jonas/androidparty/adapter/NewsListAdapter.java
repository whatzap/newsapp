package jonas.androidparty.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jonas.androidparty.R;
import jonas.androidparty.networking.model.response.ResultsObject;
import jonas.androidparty.utils.TextUtils;

/**
 * class created by jonasseputis on 05/04/19
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private List<ResultsObject> newsList;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    private NewsListAdapter.NewsClickListener newsClickListener;

    @Override
    public NewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item, parent, false);
        return new NewsListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsListAdapter.ViewHolder holder, int position) {
        holder.bind(newsList.get(position));
    }

    @Override
    public int getItemCount() {
        if (newsList == null) {
            return 0;
        }
        return newsList.size();
    }

    public void setNewsList(List<ResultsObject> newsList) {
        this.newsList = newsList;
    }

    public void setNewsItemClickListener(NewsListAdapter.NewsClickListener newsClickListener) {
        this.newsClickListener = newsClickListener;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_list_item_layout)
        public ConstraintLayout newsItemLayout;
        @BindView(R.id.topic)
        public TextView topic;
        @BindView(R.id.article_published_date)
        public TextView articlePublishedDate;
        @BindView(R.id.author_layout)
        public ConstraintLayout authorLayout;
        @BindView(R.id.article_author_value)
        public TextView articleAuthor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final ResultsObject resultsObject) {
            Log.d("bind", resultsObject.getWebTitle());
            topic.setText(resultsObject.getWebTitle());
            if (resultsObject.getWebPublicationDate() != null)
                articlePublishedDate.setText(dateFormat.format(resultsObject.getWebPublicationDate()));

            if (resultsObject.getReferences() != null && resultsObject.getReferences().size() > 0 &&
                    !TextUtils.isTextIsEmptyOrNull(resultsObject.getReferences().get(0).getAuthorId())) {
                authorLayout.setVisibility(View.VISIBLE);
                articleAuthor.setText(resultsObject.getReferences().get(0).getAuthorId());
            }

            newsItemLayout.setOnClickListener(v -> newsClickListener.onNewsItemClick(resultsObject));
        }
    }

    public interface NewsClickListener {
        void onNewsItemClick(ResultsObject resultsObject);
    }
}
